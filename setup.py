#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 15:05:52 2018

@author: ktp
"""

from setuptools import setup, find_packages


REQUIRED_PACKAGES = [
    'redis==2.10.6'
]

setup(name='frcommonutils',
      version='1.0.7',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/darkside-team/fr-commonutils',
      include_package_data=True,
      description='Face Recognition Service utilities',
      author='Ktawut T.Pijarn',
      author_email='ktawut@absoroute.io',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)


