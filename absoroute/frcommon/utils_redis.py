#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 08:24:30 2018

@author: ktp
"""

import redis
import absoroute.frcommon.settings_utils_redis as sur

from redis.sentinel import Sentinel
from arcommon.applog_utils import AppLogger


STR_FORMAT = "utf-8"
SENTINEL_SOCK_TIMEOUT = 5
    

class ArRedis():
    """
    Base class for a Redis / Sentinel connection for Absoroute modules.
    """
    
    
    def __init__(self, host, port, master_name=None, sentinel_mode=True, sentinel_timeout=SENTINEL_SOCK_TIMEOUT):
        """
        An object of this class creates and maintains a connection to Redis 
        Sentinel cluster at given host and port. The master / slave connectivity
        is handled internally.
        
        Params:
            host - Sentinel host
            port - Sentinel port
            master_name - Sentinel master's name, ignored if sentinel_mode = False
            sentinel_mode - Connect in Sentinel mode if True, basic Redis mode
                    if False
        """
        self._logger = AppLogger(name=__name__, applog_level=sur.APPLOG_LEVEL)
        
        if sentinel_mode:
            self._logger.info("Initiating Redis Sentinel connection to %s @ %s:%i" % (master_name, host, port))
            
            # TODO maybe we should create a dedicated master / slave connections
            # for write / read operations seperately? See
            # https://github.com/andymccurdy/redis-py#sentinel-support
            sentinel = Sentinel([(host, port)], socket_timeout=sentinel_timeout)
            self._redis = sentinel.master_for(master_name, socket_timeout=sentinel_timeout)
        else:
            self._logger.info("Initiating Redis connection to %s:%i" % (host, port))
            self._redis = redis.StrictRedis(host=host, port=port, db=0)
            
            

class FrRedis(ArRedis):
    """
    Redis utils for FaceRecognition modules.
    """
    
    
    KEY_PATTERN_A = "FR.%s.{%s}.%s." + sur.KEY_EMB
    KEY_PATTERN_B = "FR.%s.{%s}.%s." + sur.KEY_SIGHTING
    KEY_PATTERN_C = "FR.%s." + sur.KEY_PARTITION + "." + sur.KEY_FACE + ".%s." + sur.KEY_GROUP
    KEY_PATTERN_D = "FR.%s." + sur.KEY_PARTITION + "." + sur.KEY_GROUP + ".%s." + sur.KEY_URL
    KEY_PATTERN_E = "FR.%s." + sur.KEY_PARTITION + "." + sur.KEY_GROUP + ".%s." + sur.KEY_COUNT
    KEY_PATTERN_F = "FR.%s." + sur.KEY_PARTITION + "." + sur.KEY_GROUP + ".%s." + sur.KEY_STATUS
    
    OP_MODE_DYNAMIC = "dynamic"
    FIRST_PARTITION_NUMBER = 1
    
    
    def __init__(
            self, host, port, master_name=None, sentinel_mode=True, 
            sentinel_timeout=SENTINEL_SOCK_TIMEOUT):
        """
        Params:
            host - Sentinel host
            port - Sentinel port
            master_name - Sentinel master's name, ignored if sentinel_mode = False
            sentinel_mode - Connect in Sentinel mode if True, basic Redis mode
                    if False
        """
        super().__init__(
                host, 
                port, 
                master_name=master_name, 
                sentinel_mode=sentinel_mode, 
                sentinel_timeout=sentinel_timeout)
        
        
    def _decode_list_of_embedding(self, face_ids, emb_list):
        """
        Params:
            face_ids - a list of face_id's
            emb_list - a list of lists corresponding to faceIds, each containing 
                       embedding whose elements are represented as bytes
        Returns:
            map between face_id and embedding (list of numbers), or an empty map 
            if emb_list is a list of []
        """
        embedding_map = {}
    
        for i in range(len(emb_list)):
            emb_bytes = emb_list[i]
    
            if emb_bytes != []:
                face_id = face_ids[i]
                emb_str = list(map(lambda x: x.decode(STR_FORMAT), emb_bytes))
                emb_float = list(map(float, emb_str))
                embedding_map[face_id] = emb_float
            else:
                self._logger.warning("Found an empty embedding")
                pass
    
        return embedding_map
        

    def get_embeddings_by_partition_name(self, user_id, partition_name):
        """
        Get a map between all face_ids and their embeddings under the given 
        partition_name
        
        Returns:
            map between face_id and embedding (list of numbers), or an empty map 
            if emb_list is a list of []
        """
        key_pattern = self.KEY_PATTERN_A % (user_id, partition_name, "*")
        self._logger.debug("Getting a map of all faces / embeddings from key %s" % key_pattern)
        pipe = self._redis.pipeline()
        face_ids = []
        cursor = 0
        
        # scan() the redis keys until the cursor returns 0 to get all face_ids 
        # in the given partition_key
        while True:
            (cursor, key_list) = self._redis.scan(cursor, match=key_pattern)
            
            if (len(key_list) > 0):
                for key_b in key_list:
                    key_str = key_b.decode(STR_FORMAT)
                    face_id = key_str.split(".")[3]
                    face_ids.append(face_id)
                    pipe.lrange(key_str, 0, -1)
                
            if (cursor == 0):
                self._logger.debug("Found %i faces under key %s" % (len(face_ids), key_pattern))
                break
        
        # now get the corresponding embeddings and build the map
        rsp = pipe.execute()
        return self._decode_list_of_embedding(face_ids, rsp)
    
    
    def get_embedding_by_face_id(self, user_id, partition_name, face_id):
        """
        Get an embedding for the given face_id
        
        Params
            partition_name      The name of the partition, can be any string.
        Returns
            a list of numbers (embedding), or None if no embedding is found
        """
        key_pattern = self.KEY_PATTERN_A % (user_id, partition_name, face_id)
        self._logger.debug("Getting embedding from key %s" % key_pattern)
        rsp = self._redis.lrange(key_pattern, 0, -1)
        
        if rsp != []:
            emb_str = list(map(lambda x: x.decode(STR_FORMAT), rsp))
            return list(map(float, emb_str))
        else:
            return None
        
        
    def create_or_update_partition_by_face_id(self, user_id, face_id, partition_name):
        """
        Create or update partition's name that the given face_id is allocated to.
        """
        key_pattern = self.KEY_PATTERN_C % (user_id, face_id)
        self._logger.debug("Creating / updating partition name from key %s to %s" % (key_pattern, partition_name))
        self._redis.set(key_pattern, partition_name)
    
    
    def get_partition_by_face_id(self, user_id, face_id):
        """
        Get the partition's name that the given face_id is allocated to, None if
        not found.
        """
        key_pattern = self.KEY_PATTERN_C % (user_id, face_id)
        self._logger.debug("Getting partition name from key %s" % key_pattern)
        group = self._redis.get(key_pattern)
        
        if group:
            return group.decode(STR_FORMAT)
        else:
            return None
        
    
    def create_or_update_embedding_by_face_id(self, user_id, partition_name, face_id, embedding):
        """
        Create or update existing embedding of specified face_id. 
        
        Params:
            embedding   An ndarray or list of shape (embedding_size, )
        """
        key_pattern = self.KEY_PATTERN_A % (user_id, partition_name, face_id)
        self._logger.debug("Updating embedding at key %s" % key_pattern)
        pipe = self._redis.pipeline()
        pipe.delete(key_pattern)
        
        # insert each element in the embedding into a list. Each element will be stored as string
        for element in embedding:
            pipe.rpush(key_pattern, element)
    
        pipe.execute()
        
        
    def get_sighting_by_face_id(self, user_id, face_id, partition_name=None):
        """
        Get sighting count for the given face_id
        
        Params
            user_id
            face_id
            partition_name The name of the partition. If not specified, it will 
                           look for the correct partition storing the face_id 
                           first. 
        Returns:
            sighting count, None if not found
        """
        if partition_name:
            key_pattern = self.KEY_PATTERN_B % (user_id, partition_name, face_id)
        else:
            correct_partition_name = self.get_partition_by_face_id(user_id, face_id)
            key_pattern = self.KEY_PATTERN_B % (user_id, correct_partition_name, face_id)
            
        self._logger.debug("Getting sighting counter at key %s" % key_pattern)
        count_str = self._redis.get(key_pattern)
        
        if count_str:
            count = int(count_str.decode(STR_FORMAT))
            return count
        else:
            return None
        
        
    def adjust_total_sighting(self, user_id, face_id, partition_name=None):
        """
        Adjust sighting information of face_id and return the previous value 
        before incrementing
        
        Params:
            user_id
            face_id
            partition_name The name of the partition. If not specified, it will 
                           look for the correct partition storing the face_id 
                           first. 
        Returns:
            previous value before incrementing
        """
        if partition_name:
            key_pattern = self.KEY_PATTERN_B % (user_id, partition_name, face_id)
        else:
            correct_partition_name = self.get_partition_by_face_id(user_id, face_id)
            key_pattern = self.KEY_PATTERN_B % (user_id, correct_partition_name, face_id)
            
        self._logger.debug("Incrementing sighting counter at key %s" % key_pattern)
        prev_count = self._redis.incr(key_pattern) - 1
        return prev_count
        
    
    def get_all_partitions(self, user_id):
        """
        Get a list of all partitions of user_id. Note that it will scan the table E
        for a list of all existing partitions, then get status information from
        table F later on. 
        
        Params:
            user_id
        Returns:
            A list of partitions, each represented as a map with name, count and
            status keys. An empty list if user_id doesn't exist.
        """
        key_pattern = self.KEY_PATTERN_E % (user_id, "*")
        pipe = self._redis.pipeline()
        partition_names = []
        cursor = 0
        
        # scan() the redis keys until the cursor returns 0 to get all partition
        # names. Then we'll get their counts and statuses later.
        while True:
            (cursor, key_list) = self._redis.scan(cursor, match=key_pattern)
            
            if (len(key_list) > 0):
                for key_b in key_list:
                    key_str = key_b.decode(STR_FORMAT)
                    partition_name = key_str.split(".")[4]
                    partition_names.append(partition_name)
                    
                    key_count = self.KEY_PATTERN_E % (user_id, partition_name)
                    pipe.get(key_count)
                    
                    key_status = self.KEY_PATTERN_F % (user_id, partition_name)
                    pipe.get(key_status)
                    self._logger.debug("Getting partition details from keys %s, %s" % (key_count, key_status))
                
            if (cursor == 0):
                self._logger.debug("Found %i partitions under key %s" % (len(partition_names), key_pattern))
                break
    
        # now get the corresponding partition details and build the map
        rsp = pipe.execute()
        result = []
        
        for i in range(len(partition_names)):
            partition_map = {}
            partition_map["name"] = partition_names[i]
            partition_map["count"] = int(rsp[2 * i].decode(STR_FORMAT))
            partition_map["status"] = rsp[(2 * i) + 1].decode(STR_FORMAT)
            result.append(partition_map)
            
        return result
        
    
    def create_or_update_frsearcher_url(self, user_id, partition_name, url):
        """
        Create or update FRSearcher's URL 
        """
        key_pattern = self.KEY_PATTERN_D % (user_id, partition_name)
        self._logger.debug("Setting key %s to %s" % (key_pattern, url))
        self._redis.set(key_pattern, url)
        
        
    def get_frsearcher_url_by_partition_name(self, user_id, partition_name):
        """
        Get FRSearcher's URL by partition name
        
        Returns:
            URL of the FRSearcher, None if not found
        """
        key_pattern = self.KEY_PATTERN_D % (user_id, partition_name)
        self._logger.debug("Getting FRSearcher's URL from key %s" % key_pattern)
        url = self._redis.get(key_pattern)
        
        if url:
            return url.decode(STR_FORMAT)
        else:
            return None
    
    
    def adjust_partition_member_count(self, user_id, partition_name, count=None):
        """
        Adjust partition_name's member count
        
        Params:
            count    if None, increment from the current value by 1. If int is
                     given, set the value to be count.
        Returns:
            previous value before incrementing, or None if count is not None
        """
        key_pattern = self.KEY_PATTERN_E % (user_id, partition_name)
        if isinstance(count, int):
            self._logger.debug("Setting member counter at key %s to %i" % (key_pattern, count))
            self._redis.set(key_pattern, count)
            return None
        else:
            self._logger.debug("Incrementing member counter at key %s" % key_pattern)
            prev_count = self._redis.incr(key_pattern) - 1
            return prev_count
    
    
    def create_or_update_partition_status(self, user_id, partition_name, status):
        """
        Create or update partition to the given status (either dynamic or static)
        """
        key_pattern = self.KEY_PATTERN_F % (user_id, partition_name)
        self._logger.debug("Updating partition status at key %s to %s" % (key_pattern, status))
        
        if (status != "dynamic" and status != "static"):
            self._logger.warning("Invalid status %s" % status)
            return
        
        self._redis.set(key_pattern, status)
        
        
    def delete_face_by_face_id(self, user_id, face_id):
        """
        Delete a face by face_id along with any associated data. This includes
        deleting records in table A, B, C and decrement total face count under
        a partition face_id belongs to in table E. 
        
        Returns:
            Number of records deleted / modified in Redis
        """
        self._logger.debug("Deleting face %s and its associated data" % face_id)
        partition_name = self.get_partition_by_face_id(user_id, face_id)
        total_deleted = 0
        
        if not partition_name:
            return total_deleted
        
        key_pattern_a = self.KEY_PATTERN_A % (user_id, partition_name, face_id)
        key_pattern_b = self.KEY_PATTERN_B % (user_id, partition_name, face_id)
        key_pattern_c = self.KEY_PATTERN_C % (user_id, face_id)
        key_pattern_e = self.KEY_PATTERN_E % (user_id, partition_name)
        
        pipe = self._redis.pipeline()
        pipe.delete(key_pattern_a)
        pipe.delete(key_pattern_b)
        pipe.delete(key_pattern_c)
        pipe.decr(key_pattern_e)
        result = pipe.execute()
        
        # If delete() succeeded, it returns 1. If decr() succeeded, it returns the
        # new decreased value. We can just count how many items have value greater
        # than 0.
        total_deleted = sum(list(map(lambda x: True if x > 0 else False, result)))
        return total_deleted
    
    
    def get_partition_name(self, running_number):
        """
        A util to get a correctly-formatted partition name given the running number
        """
        return "%s%i" % (sur.KEY_FACEGROUP, int(running_number))
        
        
    def initialize_persistent(self, user_id):
        """
        Wipe existing data for FaceRecognition service clean and initialize 
        Redis to contain only 1 empty dynamic partion to begin with. Essentially 
        create just 1 record in table E and F for a dynamic partition of size 0.
        """
        self._logger.debug("Initializing Redis with 1 dynamic partition")
        key_pattern = "FR.*"
        keys = self._redis.keys(key_pattern)

        for key in keys:
            key_str = key.decode(STR_FORMAT)
            self._redis.delete(key_str)
            
        partition_name = self.get_partition_name(self.FIRST_PARTITION_NUMBER)
        self.create_or_update_partition_status(user_id, partition_name, self.OP_MODE_DYNAMIC)
        self.adjust_partition_member_count(user_id, partition_name, 0)
        
        
class OtRedis(ArRedis):
    """
    Redis util for Object Tracker modules
    """
    
    
    KEY_PATTERN_G = "OT.%s.%s"
    STATUS_ACTIVE = "active"
    
    
    def __init__(
            self, host, port, master_name=None, sentinel_mode=True, 
            sentinel_timeout=SENTINEL_SOCK_TIMEOUT):
        """
        Params:
            host - Sentinel host
            port - Sentinel port
            master_name - Sentinel master's name, ignored if sentinel_mode = False
            sentinel_mode - Connect in Sentinel mode if True, basic Redis mode
                    if False
        """
        super().__init__(
                host, 
                port, 
                master_name=master_name, 
                sentinel_mode=sentinel_mode, 
                sentinel_timeout=sentinel_timeout)
        

    def set_job_properties(self, user_id, job_id, properties, set_partial_fields=False):
        """
        Create or update job's properties for the given user_id and job_id
        
        Params:
            properties          A map of job's properties
            set_partial_fields  If True, allow setting only some fields (no 
                                missing-field validation is done). If False, will
                                validate that all mandatory fields exist in the
                                given properties dict.
        """
        if not isinstance(properties, dict):
            raise ValueError("properties must be dict")
            
        if len(properties) == 0:
            raise ValueError("properties contain at least 1 k-v pair")
            
        if not set_partial_fields:
            keys = properties.keys()
            if (sur.KEY_STREAM_URL not in keys) or \
                (sur.KEY_CALLBACK_URL not in keys) or \
                (sur.KEY_KEYFRAME_FREQ not in keys) or \
                (sur.KEY_DRAW_DETECTION not in keys) or \
                (sur.KEY_STATUS not in keys) or \
                (sur.KEY_CAMID not in keys) or \
                (sur.KEY_CALLBACK_FREQ not in keys) or \
                (sur.KEY_OUTPUT_FPS not in keys) or \
                (sur.KEY_UPLOAD_ON_MOVEMENT not in keys) or \
                (sur.KEY_STATUS_MSG not in keys) or \
                (sur.KEY_OUTPUT_LOCATION not in keys):
                    
                raise ValueError("key missing")
                
        key = self.KEY_PATTERN_G % (user_id, job_id)
        self._logger.debug("Updating job properties at key %s with value %s" % (key, properties))
        self._redis.hmset(key, properties)
        
    
    def _str_to_bool(self, val):
        if type(val) == bool:
            return val
        elif val.casefold() == "true":
            return True
        elif val.casefold() == "false":
            return False
        else:
            raise ValueError("Cannot parse %s to boolean" % val)
        
        
    def get_job_properties(self, user_id, job_id):
        """
        Get job's properties for the given user_id and job_id, with the correct
        data types in respective fields.
        
        Returns:
            properties dict, an empty dict if not found
        """
        key = self.KEY_PATTERN_G % (user_id, job_id)
        self._logger.debug("Getting job properties at key %s" % (key))
        properties_b = self._redis.hgetall(key)
        properties = {}
        
        # try to cast correct data type to known fields
        for kb, vb in properties_b.items():
            k = kb.decode(STR_FORMAT)
            v = vb.decode(STR_FORMAT)
            
            if (v == "None"):
                properties[k] = None
            elif ((k == sur.KEY_KEYFRAME_FREQ) or (k == sur.KEY_CALLBACK_FREQ) or
                (k == sur.KEY_OUTPUT_FPS)):
                properties[k] = int(v)
            elif ((k == sur.KEY_DRAW_DETECTION) or (k == sur.KEY_UPLOAD_ON_MOVEMENT)):
                properties[k] = self._str_to_bool(v)
            else:
                properties[k] = v
                
        return properties
        
        
    def get_all_jobs_by_user_id(self, user_id):
        """
        Get a list of all job_id's by user_id
        
        Returns:
            a list of job_id's, an empty list if user_id has no job
        """
        key_pattern = self.KEY_PATTERN_G % (user_id, "*")
        self._logger.debug("Getting all job IDs at key %s" % (key_pattern))
        cursor = 0
        job_ids = []
        
        # scan() the redis keys until the cursor returns 0 to get all keys.
        while True:
            (cursor, key_list) = self._redis.scan(cursor, match=key_pattern)
            
            if (len(key_list) > 0):
                for key_b in key_list:
                    key_str = key_b.decode(STR_FORMAT)
                    job_id = key_str.split(".")[2]
                    job_ids.append(job_id)
                    
            if (cursor == 0):
                self._logger.debug("Found %i jobs under key %s" % (len(job_ids), key_pattern))
                break
    
        return job_ids
    
    
    def job_exists_by_user_id_and_stream_url(self, user_id, streaming_url, status=STATUS_ACTIVE):
        """
        Check if there is an existing job for user_id and streaming_url with
        status.
        
        Returns:
            True if at least 1 job matching criteria exists, False otherwise
        """
        self._logger.debug("Checking if a job for user %s, stream %s with status %s exists" % (user_id, streaming_url, status))
        job_ids = self.get_all_jobs_by_user_id(user_id)
        
        for job_id in job_ids:
            job_prop = self.get_job_properties(user_id, job_id)
            job_stream = job_prop[sur.KEY_STREAM_URL]
            job_status = job_prop[sur.KEY_STATUS]
            
            if (job_status == status) and (job_stream == streaming_url):
                self._logger.debug("Job found")
                return True
            else:
                pass
        
        return False
    
    
    def initialize_persistent_by_user_id(self, user_id):
        """
        Wipe existing data for Object Tracker service clean
        
        Params:
            user_id     The user_id to clean all job data from. If it's "*", it
                        will effectively clean all job data from all users. 
        """
        self._logger.debug("Initializing Redis for Object Tracker service")
        key_pattern = self.KEY_PATTERN_G % (user_id, "*")
        keys = self._redis.keys(key_pattern)

        for key in keys:
            key_str = key.decode(STR_FORMAT)
            self._redis.delete(key_str)
                
            
        
        
        
        
        
        
        
        
        
        