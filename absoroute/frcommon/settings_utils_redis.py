#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 12:13:07 2018

@author: ktp
"""

import os

# logging
LOG_LEVEL = int(os.getenv("LOG_LEVEL", 10))
APPLOG_LEVEL = int(os.getenv("APPLOG_LEVEL", 4))    # 0 - Debug, 4 - Critical

# keyspace
KEY_FACEGROUP = "faceGroup"
KEY_PARTITION = "partitionMap"
KEY_EMB = "emb"
KEY_FACE = "face"
KEY_SIGHTING = "sighting"
KEY_GROUP = "group"
KEY_URL = "url"
KEY_COUNT = "count"
KEY_STATUS = "status"

# keys for OT job status
KEY_STREAM_URL = "streaming_url"
KEY_CALLBACK_URL = "callback_url"
KEY_CALLBACK_FREQ = "callback_frequency"
KEY_KEYFRAME_FREQ = "keyframe_frequency"
KEY_DRAW_DETECTION = "draw_detection"
KEY_OUTPUT_LOCATION = "output_location"
KEY_OUTPUT_FPS = "output_fps"
KEY_UPLOAD_ON_MOVEMENT = "upload_on_movement"
KEY_STATUS = "status"
KEY_CAMID = "cam_id"
KEY_STATUS_MSG = "message"

# test configurations, pointing to Redis Sentinel cluster locally
TEST_REDIS_SENTINEL_HOST = os.getenv("TEST_REDIS_SENTINEL_HOST", "127.0.0.1")
TEST_REDIS_SENTINEL_PORT = int(os.getenv("TEST_REDIS_SENTINEL_PORT", 26379))
TEST_REDIS_SENTINEL_MASTER_NAME = os.getenv("TEST_REDIS_SENTINEL_MASTER_NAME", "mymaster")