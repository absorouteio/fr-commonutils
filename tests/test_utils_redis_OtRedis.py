#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  1 22:34:12 2018

@author: ktp
"""

import unittest as ut
import absoroute.frcommon.utils_redis as urd
import absoroute.frcommon.settings_utils_redis as sur


def delete_all_keys(OtRedis):
    keys = OtRedis._redis.keys()

    for key in keys:
        key_str = key.decode(urd.STR_FORMAT)
#        print("Deleting key %s" % (key_str))
        OtRedis._redis.delete(key_str)


class TestOtRedis(ut.TestCase):
    
    
    @classmethod
    def setUpClass(cls):
        cls.maxDiff = None
    
    
    def setUp(self):
        self.otr = urd.OtRedis(
                host=sur.TEST_REDIS_SENTINEL_HOST, 
                port=sur.TEST_REDIS_SENTINEL_PORT,
                master_name=sur.TEST_REDIS_SENTINEL_MASTER_NAME, 
                sentinel_mode=True)
    
    
    def tearDown(self):
        delete_all_keys(self.otr)
        
        
    def test_create_read_update_job_properties(self):
        
        # can get job IDs when nothing exists
        job_ids = self.otr.get_all_jobs_by_user_id("test_user")
        self.assertEqual(len(job_ids), 0)
        
        properties = {
                sur.KEY_STREAM_URL: "stream_url",
                sur.KEY_CALLBACK_URL: "callback_url",
                sur.KEY_KEYFRAME_FREQ: 30,
                sur.KEY_DRAW_DETECTION: True,
                sur.KEY_OUTPUT_LOCATION: "somewhere",
                sur.KEY_STATUS: "active",
                sur.KEY_CALLBACK_FREQ: 30,
                sur.KEY_OUTPUT_FPS: 30,
                sur.KEY_UPLOAD_ON_MOVEMENT: True,
                sur.KEY_CAMID: "cam1234",
                sur.KEY_STATUS_MSG: None
        }
        # can set properties
        self.otr.set_job_properties("test_user", "job_1234", properties)
        
        # can get job IDs
        job_ids = self.otr.get_all_jobs_by_user_id("test_user")
        self.assertEqual(len(job_ids), 1)
        self.assertEqual(job_ids[0], "job_1234")
        
        # can get properties
        rst_properties = self.otr.get_job_properties("test_user", "job_1234")
        self.assertEqual(properties, rst_properties)
        
        # can update properties
        properties[sur.KEY_STATUS]: "inactive"
        self.otr.set_job_properties("test_user", "job_1234", properties)
        rst_properties = self.otr.get_job_properties("test_user", "job_1234")
        self.assertEqual(properties, rst_properties)
        
        # can partially update properties
        properties[sur.KEY_UPLOAD_ON_MOVEMENT] = False
        self.otr.set_job_properties(
                "test_user", "job_1234", {sur.KEY_UPLOAD_ON_MOVEMENT: False}, 
                set_partial_fields=True)
        rst_properties = self.otr.get_job_properties("test_user", "job_1234")
        self.assertEqual(properties, rst_properties)
        
        # can handle none-existing jobs
        self.assertEqual(self.otr.get_job_properties("test_user", "blaaaa"), {})
        self.assertEqual(self.otr.get_job_properties("blaaaa", "job_1234"), {})
        
    
    def test_initialize_redis_for_ot(self):
        
        # insert non-OT entry
        self.otr._redis.set("test", "value")
        
        # insert OT entry
        properties = {
                sur.KEY_STREAM_URL: "stream_url",
                sur.KEY_CALLBACK_URL: "callback_url",
                sur.KEY_KEYFRAME_FREQ: 30,
                sur.KEY_DRAW_DETECTION: True,
                sur.KEY_OUTPUT_LOCATION: "somewhere",
                sur.KEY_STATUS: "active",
                sur.KEY_CALLBACK_FREQ: 30,
                sur.KEY_OUTPUT_FPS: 30,
                sur.KEY_UPLOAD_ON_MOVEMENT: True,
                sur.KEY_CAMID: "cam1234",
                sur.KEY_STATUS_MSG: "started normally"
        }
        self.otr.set_job_properties("test_user_1", "job_1234", properties)
        self.otr.set_job_properties("test_user_2", "job_1234", properties)
        self.otr.set_job_properties("test_user_3", "job_1234", properties)
        
        # init for test_user_1
        self.otr.initialize_persistent_by_user_id("test_user_1")
        job_ids_1 = self.otr.get_all_jobs_by_user_id("test_user_1")
        job_ids_2 = self.otr.get_all_jobs_by_user_id("test_user_2")
        job_ids_3 = self.otr.get_all_jobs_by_user_id("test_user_3")
        self.assertEqual(len(job_ids_1), 0)
        self.assertEqual(len(job_ids_2), 1)
        self.assertEqual(len(job_ids_3), 1)
        
        # init for all
        self.otr.initialize_persistent_by_user_id("*")
        job_ids_1 = self.otr.get_all_jobs_by_user_id("test_user_1")
        job_ids_2 = self.otr.get_all_jobs_by_user_id("test_user_2")
        job_ids_3 = self.otr.get_all_jobs_by_user_id("test_user_3")
        self.assertEqual(len(job_ids_1), 0)
        self.assertEqual(len(job_ids_2), 0)
        self.assertEqual(len(job_ids_3), 0)
        
        # non-OT data must not be affected
        rst = self.otr._redis.get("test")
        self.assertEqual(rst, b"value")
        
        
    def test_job_exists_by_user_id_and_stream_url(self):
        
        # insert OT entry
        self.otr.set_job_properties("test_user_1", "job_1234", {sur.KEY_STREAM_URL: "stream_url_1", sur.KEY_STATUS: "active"}, True)
        self.otr.set_job_properties("test_user_1", "job_2345", {sur.KEY_STREAM_URL: "stream_url_2", sur.KEY_STATUS: "active"}, True)
        self.otr.set_job_properties("test_user_1", "job_3456", {sur.KEY_STREAM_URL: "stream_url_3", sur.KEY_STATUS: "inactive"}, True)
        
        self.assertTrue(self.otr.job_exists_by_user_id_and_stream_url("test_user_1", "stream_url_1"))
        self.assertFalse(self.otr.job_exists_by_user_id_and_stream_url("test_user_1", "stream_url_1", "inactive"))
        
        self.assertFalse(self.otr.job_exists_by_user_id_and_stream_url("test_user_1", "stream_url_3"))
        self.assertTrue(self.otr.job_exists_by_user_id_and_stream_url("test_user_1", "stream_url_3", "inactive"))
        
        
if __name__ == '__main__':
    ut.main()
        
        
        
        
        
        