#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  7 22:29:40 2018

@author: ktp
"""

import uuid
import unittest as ut
import numpy as np
import absoroute.frcommon.utils_redis as urd
import absoroute.frcommon.settings_utils_redis as sur


def prepare_test_data(persistent_redis, user_id, no_groups, no_emb_per_group, 
                       emb_size, min_sighting=1, max_sighting=5):
    
#    print("Creating %i groups with %i embeddings of size %i each" % (no_groups, no_emb_per_group, emb_size))
    pipe = persistent_redis._redis.pipeline()
    emb_0 = []
    face_id_0 = None
    sighting_0 = None
    face_id_1 = None
    
    for group in range(no_groups):
        group_name = "faceGroup%i" % (group)
#        print("Creating group %s" % group_name)
        
        for person in range(no_emb_per_group):
            # face embedding (A)
            face_id = str(uuid.uuid4())
            key_emb = "FR.%s.{%s}.%s.emb" % (user_id, group_name, face_id)
            embedding = np.random.randn(emb_size)
            
            for element in embedding:
                pipe.rpush(key_emb, element)
                
            # (B)
            key_sighting = "FR.%s.{%s}.%s.sighting" % (user_id, group_name, face_id)
            sighting = np.random.randint(min_sighting, max_sighting)
            
            for i in range(sighting):
                pipe.incr(key_sighting)
                
            # (C)
            key_group_map = "FR.%s.partitionMap.face.%s.group" % (user_id, face_id)
            pipe.set(key_group_map, group_name)
            pipe.execute()
            
            if (group == 0 and person == 0):
                emb_0 = embedding
                face_id_0 = face_id
                sighting_0 = sighting
                
            if (group == 0 and person == 1):
                face_id_1 = face_id
                
            # (E)
            key_group_count = "FR.%s.partitionMap.group.%s.count" % (user_id, group_name)
            pipe.incr(key_group_count)
            
        # (D)
        key_group_url = "FR.%s.partitionMap.group.%s.url" % (user_id, group_name)
        group_url = "127.0.0.1:6789/frsearch%i" % (group)
        pipe.set(key_group_url, group_url)
        pipe.execute()
        
        # (F)
        key_group_status = "FR.%s.partitionMap.group.%s.status" % (user_id, group_name)
        if (group < no_groups - 1):
            pipe.set(key_group_status, "static")
        else:
            pipe.set(key_group_status, "dynamic")
            
    pipe.execute()
        
    # return an example values for later uses in the tests
    return face_id_0, emb_0, sighting_0, face_id_1


def delete_all_keys(persistent_redis):
    keys = persistent_redis._redis.keys()

    for key in keys:
        key_str = key.decode(urd.STR_FORMAT)
#        print("Deleting key %s" % (key_str))
        persistent_redis._redis.delete(key_str)


class TestFrRedis(ut.TestCase):
    
    # TODO use key configs in sur instead of hard coding stuff
    
    def setUp(self):
        self.prd = urd.FrRedis(
                host=sur.TEST_REDIS_SENTINEL_HOST, port=sur.TEST_REDIS_SENTINEL_PORT,
                master_name=sur.TEST_REDIS_SENTINEL_MASTER_NAME, sentinel_mode=True)
        self.user_id = "TEST_USER1"
        self.total_groups= 5
        self.emb_per_group = 10
        self.emb_size = 15
        (self.face_id_0, self.emb_0, self.sighting_0, self.face_id_1) = prepare_test_data(
                self.prd, self.user_id, self.total_groups, self.emb_per_group, 
                self.emb_size)


    def tearDown(self):
        delete_all_keys(self.prd)
        
        
    def test_decode_list_of_embedding_valid_emb(self):
        emb_list = [
            [b"1", b"2", b"3"],
            [b"3", b"2", b"1"]
        ]
        faceIds = ["faceId1", "faceId2"]
        emb_map = self.prd._decode_list_of_embedding(faceIds, emb_list)
        
        self.assertEqual(emb_map["faceId1"], [1, 2, 3])
        self.assertEqual(emb_map["faceId2"], [3, 2, 1])
        
    
    def test_decode_list_of_embedding_empty_emb(self):
        emb_list = [[]]
        faceIds = []
        emb_map = self.prd._decode_list_of_embedding(faceIds, emb_list)
        
        self.assertEqual(emb_map, {})
        
        
    def test_get_embeddings_by_partition_name(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        group0_faces = self.prd.get_embeddings_by_partition_name(self.user_id, partition_name)
        
        self.assertEqual(len(group0_faces), self.emb_per_group)
        self.assertTrue(self.face_id_0 in group0_faces.keys())
        self.assertTrue((group0_faces[self.face_id_0] == self.emb_0).all())
        
        
    def test_create_or_update_partition_by_face_id(self):
        user_id = "test_user"
        face_id = "test_face"
        partition_name = "test_partition"
        self.prd.create_or_update_partition_by_face_id(user_id, face_id, partition_name)
        key_name = self.prd.KEY_PATTERN_C % (user_id, face_id)
        rst = self.prd._redis.get(key_name).decode(urd.STR_FORMAT)
        
        self.assertEqual(rst, partition_name)
        
        
    def test_get_partition_by_face_id_exist(self):
        group = self.prd.get_partition_by_face_id(self.user_id, self.face_id_0)
        exp = sur.KEY_FACEGROUP + "0"
        
        self.assertEqual(group, exp)
        
    
    def test_get_partition_by_face_id_not_exist(self):
        group = self.prd.get_partition_by_face_id(self.user_id, "blaaaa")
        
        self.assertIsNone(group)
        
        
    def test_get_sighting_by_face_id_no_partition_name(self):
        count = self.prd.get_sighting_by_face_id(self.user_id, self.face_id_0)
        
        # we can only check gte since other tests might have incremented the counter already
        self.assertTrue(count >= self.sighting_0)
        
        
    def test_get_sighting_by_invalid_face_id_no_partition_name(self):
        count = self.prd.get_sighting_by_face_id(self.user_id, "invalid_face_id")
        
        self.assertIsNone(count)
        
    
    def test_adjust_total_sighting(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        prv_count_1 = self.prd.adjust_total_sighting(self.user_id, self.face_id_0, partition_name)
        prv_count_2 = self.prd.adjust_total_sighting(self.user_id, self.face_id_0, partition_name)
        
        self.assertEqual(prv_count_2, prv_count_1 + 1)
        
        
    def test_adjust_total_sighting_no_partition_name(self):
        prv_count_1 = self.prd.adjust_total_sighting(self.user_id, self.face_id_0)
        prv_count_2 = self.prd.adjust_total_sighting(self.user_id, self.face_id_0)
        
        self.assertEqual(prv_count_2, prv_count_1 + 1)
        
        
    def test_get_embedding_by_face_id(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        emb = self.prd.get_embedding_by_face_id(self.user_id, partition_name, self.face_id_0)

        self.assertTrue((emb == self.emb_0).all())
        
        
    def test_get_embedding_by_face_id_not_found(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        emb = self.prd.get_embedding_by_face_id(self.user_id, partition_name, "blaaaa")
        
        self.assertIsNone(emb)
        
        
    def test_create_or_update_embedding_by_face_id(self):
        testUser = "testUser"
        testPartition = "testPartition"
        testFaceId = "testFaceId"
        emb1 = [1, 2, 3]
        self.prd.create_or_update_embedding_by_face_id(testUser, testPartition, testFaceId, emb1)
        r_emb1 = self.prd.get_embedding_by_face_id(testUser, testPartition, testFaceId)
        self.assertTrue(emb1 == r_emb1)
        
        emb2 = [2, 3, 4]
        self.prd.create_or_update_embedding_by_face_id(testUser, testPartition, testFaceId, emb2)
        r_emb2 = self.prd.get_embedding_by_face_id(testUser, testPartition, testFaceId)
        self.assertTrue(emb2 == r_emb2)
        
        
    def test_get_all_partitions(self):
        result = self.prd.get_all_partitions(self.user_id)
        
        for partition in result:
            self.assertTrue("name" in partition.keys())
            self.assertTrue("count" in partition.keys())
            self.assertTrue("status" in partition.keys())
        
        self.assertEqual(len(result), self.total_groups)
        
        
    def test_get_all_partitions_invalid_user_id(self):
        result = self.prd.get_all_partitions("invalid_user")
        
        self.assertEqual(len(result), 0)
        
        
    def test_adjust_partition_member_count(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        result1 = self.prd.adjust_partition_member_count(self.user_id, partition_name)
        self.assertEqual(result1, self.emb_per_group)
        
        result2 = self.prd.adjust_partition_member_count(self.user_id, partition_name)
        self.assertEqual(result2, self.emb_per_group + 1)
        
        
    def test_adjust_partition_member_count_set_value(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        result1 = self.prd.adjust_partition_member_count(self.user_id, partition_name, 0)
        self.assertEqual(result1, None)
        
        result2 = self.prd.adjust_partition_member_count(self.user_id, partition_name)
        self.assertEqual(result2, 0)
        
        
    def test_update_partition_status(self):
        partition_name = sur.KEY_FACEGROUP + "0"
        key_name = "FR.%s.%s.%s.%s.%s" % (self.user_id, sur.KEY_PARTITION, sur.KEY_GROUP, partition_name, sur.KEY_STATUS)
        prev_status = self.prd._redis.get(key_name).decode(urd.STR_FORMAT)
        self.prd.create_or_update_partition_status(self.user_id, partition_name, "dynamic")
        cur_status = self.prd._redis.get(key_name).decode(urd.STR_FORMAT)
        
        self.assertTrue(prev_status == "static")
        self.assertTrue(cur_status == "dynamic")
        
        
    def test_create_partition_status(self):
        partition_name = "blaaaa"
        key_name = "FR.%s.%s.%s.%s.%s" % (self.user_id, sur.KEY_PARTITION, sur.KEY_GROUP, partition_name, sur.KEY_STATUS)
        self.prd.create_or_update_partition_status(self.user_id, partition_name, "static")
        cur_status = self.prd._redis.get(key_name).decode(urd.STR_FORMAT)
        
        self.assertTrue(cur_status == "static")


    def test_create_or_update_frsearcher_url(self):
        partition_name = "blaaaa"
        url = "bluuuuur"
        key_name = "FR.%s.%s.%s.%s.%s" % (self.user_id, sur.KEY_PARTITION, sur.KEY_GROUP, partition_name, sur.KEY_URL)
        self.prd.create_or_update_frsearcher_url(self.user_id, partition_name, url)
        rst = self.prd._redis.get(key_name).decode(urd.STR_FORMAT)
        
        self.assertTrue(rst == url)
    
    
    def test_get_frsearcher_url_by_partition_name(self):
        partition_name = "fooooo"
        url = "baaar"
        key_name = "FR.%s.%s.%s.%s.%s" % (self.user_id, sur.KEY_PARTITION, sur.KEY_GROUP, partition_name, sur.KEY_URL)
        self.prd._redis.set(key_name, url)
        rst = self.prd.get_frsearcher_url_by_partition_name(self.user_id, partition_name)
        
        self.assertTrue(rst == url)
        
        
    def test_delete_face_by_face_id(self):
        count = self.prd.delete_face_by_face_id(self.user_id, self.face_id_1)
        
        self.assertEqual(count, 4)
        
        
    def test_delete_face_by_invalid_face_id(self):
        count = self.prd.delete_face_by_face_id(self.user_id, "invalid_face_id")
        
        self.assertEqual(count, 0)
        
        
    def test_initialize_persistent(self):
        self.prd.initialize_persistent(self.user_id)
        
        keys = self.prd._redis.keys()
        self.assertTrue(b'FR.TEST_USER1.partitionMap.group.faceGroup1.count' in keys)
        self.assertTrue(b'FR.TEST_USER1.partitionMap.group.faceGroup1.status' in keys)
        
        status = self.prd._redis.get('FR.TEST_USER1.partitionMap.group.faceGroup1.status').decode(urd.STR_FORMAT)
        self.assertEqual(status, self.prd.OP_MODE_DYNAMIC)
        
        count = int(self.prd._redis.get('FR.TEST_USER1.partitionMap.group.faceGroup1.count').decode(urd.STR_FORMAT))
        self.assertEqual(count, 0)
        
        
if __name__ == '__main__':
    ut.main()