#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 25 20:14:03 2018

@author: root
"""

import uuid
import redis
import time
import numpy as np
import absorouteutils.settings_utils_redis as sur


MIN_SIGHTING = 1
MAX_SIGHTING = 10
STR_FORMAT = "utf-8"
LOOP = 10
ALGO_EMBEDDING_SIZE = 512
ALGO_INVALID_FACEID = "invalid_faceId"
ALGO_INVALID_DISTANCE = 1000
_redis = redis.StrictRedis(host=sur.TEST_REDIS_HOST, port=sur.TEST_REDIS_PORT, db=0)


def prepare_test_data(no_groups, no_emb_per_group, emb_size):
    print("Creating %i groups with %i embeddings of size %i each" % (no_groups, no_emb_per_group, emb_size))
    pipe = _redis.pipeline()
    emb_0 = []
    face_id_0 = None
    
    for group in range(no_groups):
        group_name = "faceGroup%i" % (group)
        print("Creating group %s" % group_name)
        
        for person in range(no_emb_per_group):
            # face embedding (A)
            face_id = str(uuid.uuid4())
            key_emb = "{%s}.%s.emb" % (group_name, face_id)
            embedding = np.random.randn(emb_size)
            
            for element in embedding:
                pipe.rpush(key_emb, element)
                
            # face counter (B)
            key_sighting = "{%s}.%s.sighting" % (group_name, face_id)
            sighting = np.random.randint(MIN_SIGHTING, MAX_SIGHTING)
            
            for i in range(sighting):
                pipe.incr(key_sighting)
                
            # (C)
            key_group_map = "partitionMap.face.%s.group" % (face_id)
            pipe.set(key_group_map, group_name)
            pipe.execute()
            
            if (group == 0 and person == 0):
                emb_0 = embedding
                face_id_0 = face_id
            
        # (D)
        key_group_url = "partitionMap.group.%s.url" % (group_name)
        group_url = "127.0.0.1:6789/frsearch%i" % (group)
        pipe.set(key_group_url, group_url)
        pipe.execute()
        
    # return an example embedding / face_id for later verification
    return face_id_0, emb_0
        
        
def _decode_list_of_embedding(face_ids, emb_list):
    """
    Decode returned list of results from pipe API to string

    params:
        face_ids - a list of face_id's
        emb_list - a list of lists corresponding to faceIds, each containing embedding whose
                elements are represented as bytes
    returns:
        map between face_id and embedding (list of numbers), or an empty map if emb_list is a
                list of []
    """
    embedding_map = {}

    for i in range(len(emb_list)):
        emb_bytes = emb_list[i]

        if emb_bytes != []:
            face_id = face_ids[i]
            emb_str = list(map(lambda x: x.decode(STR_FORMAT), emb_bytes))
            emb_float = list(map(float, emb_str))
            embedding_map[face_id] = emb_float
        else:
            print("Found an empty embedding")
            pass

    return embedding_map


def get_embeddings_by_faceGroup(group_id):
    """
    Get a map between face_ids and their embeddings under the give group_id
    """
    key_pattern = "{faceGroup%i}.*.emb" % group_id
    keys = _redis.keys(key_pattern)
    pipe = _redis.pipeline()
    face_ids = []
    
    for key in keys:
        key_str = key.decode(STR_FORMAT)
        face_id = key_str.split(".")[1]
        face_ids.append(face_id)
        pipe.lrange(key_str, 0, -1)
        
    rsp = pipe.execute()
    return _decode_list_of_embedding(face_ids, rsp)
        
        
def find_nearest_face_id(embedding_map, embedding, matric="l2"):
    """
    Find the nearest matching faceId from embedding_map to the given embedding 
    using bruteforce search

    params:
        embedding_map - map between (faceId, embedding vector as list)
        embedding - target embedding to find a match
        matric - either "ls" or "cosine"
    return:
        nearest_faceId, distance
    """
    total_faces = len(embedding_map)

    if total_faces < 1:
        print("No other faces to compare against the given embedding")
        return ALGO_INVALID_FACEID, ALGO_INVALID_DISTANCE

    print("Comparing embedding against %i other faces" % (total_faces))
    # building embedding space containing all face embeddings to compare against
    all_faces = np.zeros((total_faces, ALGO_EMBEDDING_SIZE))
    i = 0

    for emb in embedding_map.values():
        all_faces[i, :] = emb
        i = i + 1

    if matric == "l2":
        #distances = np.linalg.norm(all_faces - embedding, axis=1)  # sqrt(sum(delta^2))
        distances = np.sum(np.square(all_faces - embedding), axis=1)  # sum(delta^2)
    elif matric == "l1":
        distances = np.sum(np.absolute(all_faces - embedding), axis=1)
    else:
        print("matric %s not yet supported" % (matric))
        #abort(500)

    match_index = np.argmin(distances)
    faceIds = list(embedding_map.keys())
    nearest_faceId = faceIds[match_index]
    nearest_distance = distances[match_index]
    print("Nearest faceId is %s at distance %f" % (nearest_faceId, nearest_distance))

    return nearest_faceId, nearest_distance
    

def delete_all_keys():
    keys = _redis.keys()

    for key in keys:
        key_str = key.decode(STR_FORMAT)
        print("Deleting key %s" % (key_str))
        _redis.delete(key_str)
        
        
# create test data
total_groups = 10
ppl_per_group = 1000
face_id_0, emb_0 = prepare_test_data(total_groups, ppl_per_group, 512)
print("There are %i keys in DB" % len(_redis.keys()))

# time to get all faces from a group 
# 0.185 s / 100 faces
# 1.848 s / 1000 faces
start_ts = time.time()
for i in range(LOOP):
    embedding_map = get_embeddings_by_faceGroup(0)
stop_ts = time.time()
print("Took %f seconds on average to get %i faces from group %i" % ((stop_ts - start_ts) / LOOP, ppl_per_group, 0))

# time to search for a match with L1
# 0.0024 s / 100 faces
# 0.0157 s / 1000 faces
start_ts = time.time()
for i in range(LOOP):
    nearest_face_id, nearest_distance = find_nearest_face_id(embedding_map, emb_0, matric="l1")
stop_ts = time.time()
assert(nearest_face_id == face_id_0)
print("Nearest L1 match %s at %f" % (nearest_face_id, nearest_distance))
print("Took %f seconds on average to find a match with L1" % ((stop_ts - start_ts) / LOOP))

# time to search for a match with L2
# 0.0023 s / 100 faces
# 0.0146 s / 1000 faces
start_ts = time.time()
for i in range(LOOP):
    nearest_face_id, nearest_distance = find_nearest_face_id(embedding_map, emb_0, matric="l2")
stop_ts = time.time()
assert(nearest_face_id == face_id_0)
print("Nearest L2 match %s at %f" % (nearest_face_id, nearest_distance))
print("Took %f seconds on average to find a match with L2" % ((stop_ts - start_ts) / LOOP))

# clear DB
delete_all_keys()
    



